package com.josala.demoapp.di.component;

import com.josala.demoapp.di.PerActivity;
import com.josala.demoapp.di.module.ActivityModule;
import com.josala.demoapp.view.activity.HeroDetailActivity;
import com.josala.demoapp.view.activity.HomeActivity;
import com.josala.demoapp.view.activity.SplashActivity;

import dagger.Component;

/**
 * Created by nomad on 01/04/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    /**
     * Inject.
     *
     * @param splashActivity
     */
    void inject(SplashActivity splashActivity);

    /**
     * Inject.
     *
     * @param mainActivity
     */
    void inject(HomeActivity mainActivity);

    /**
     * Inject.
     *
     * @param heroDetailActivity
     */
    void inject(HeroDetailActivity heroDetailActivity);
}
