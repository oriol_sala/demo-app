package com.josala.demoapp.di.module;

import android.support.v7.app.AppCompatActivity;

import com.josala.demoapp.business.FavouriteCharacterUseCase;
import com.josala.demoapp.business.GetCharacterByIdUseCase;
import com.josala.demoapp.business.GetCharactersUseCase;
import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.common.ImageLoader;
import com.josala.demoapp.common.Navigator;
import com.josala.demoapp.common.RealmManager;
import com.josala.demoapp.data.rest.mapper.CharacterMapper;
import com.josala.demoapp.di.PerActivity;
import com.josala.demoapp.presenter.HeroDetailPresenter;
import com.josala.demoapp.presenter.HomePresenter;
import com.josala.demoapp.presenter.SplashPresenter;
import com.josala.demoapp.view.adapter.CharactersGridAdapter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by nomad on 01/04/2017.
 */
@Module
public class ActivityModule {

    private final AppCompatActivity activity;

    /**
     * Instantiates a new Activity module.
     *
     * @param activity the context
     */
    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    SplashPresenter provideSplashPresenter(GetCharactersUseCase getCharactersUseCase,
                                           Navigator navigator) {
        return new SplashPresenter(getCharactersUseCase, navigator);
    }

    @Provides
    @PerActivity
    CharacterMapper provideCharacterMapper() {
        return new CharacterMapper();
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return new Navigator(activity);
    }

    @Provides
    @PerActivity
    HomePresenter provideHomePresenter(GetCharactersUseCase getCharactersUseCase,
                                       GetCharacterByIdUseCase getCharacterByIdUseCase,
                                       Navigator navigator) {
        return new HomePresenter(getCharactersUseCase, getCharacterByIdUseCase, navigator);
    }

    @Provides
    @PerActivity
    CharactersGridAdapter provideCharactersGridAdapter(ImageLoader imageLoader) {
        return new CharactersGridAdapter(activity, imageLoader);
    }

    @Provides
    @PerActivity
    ImageLoader provideImageLoader() {
        return new ImageLoader();
    }

    @Provides
    @PerActivity
    RealmManager provideRealmManager() {
        return new RealmManager();
    }

    @Provides
    @PerActivity
    HeroDetailPresenter provideHeroDetailPresenter(FavouriteCharacterUseCase favouriteCharacterUseCase,
                                                   RealmManager realmManager) {
        return new HeroDetailPresenter(favouriteCharacterUseCase, realmManager);
    }
}
