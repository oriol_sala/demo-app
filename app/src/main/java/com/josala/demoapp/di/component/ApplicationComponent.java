package com.josala.demoapp.di.component;

import android.content.Context;

import com.josala.demoapp.DemoApp;
import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.data.rest.ApiClient;
import com.josala.demoapp.di.module.ApplicationModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;

/**
 * Created by nomad on 01/04/2017.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    /**
     * Inject.
     *
     * @param application the application
     */
    void inject(DemoApp application);

    /**
     * Context context.
     *
     * @return the context
     */
    Context context();

    /**
     * Rest api client.
     *
     * @return
     */
    ApiClient apiClient();

    /**
     * Subscriber scheduler.
     *
     * @return the scheduler
     */
    @Named("subscriber")
    Scheduler subscriber();

    /**
     * Observer scheduler.
     *
     * @return the scheduler
     */
    @Named("observer")
    Scheduler observer();

    /**
     * Encrypt manager.
     *
     * @return
     */
    EncryptManager encryptManager();

    /**
     * Api base url.
     *
     * @return
     */
    @Named("apiUrl")
    String apiUrl();
}
