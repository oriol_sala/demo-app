package com.josala.demoapp.di.module;

import android.app.Application;
import android.content.Context;

import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.data.rest.ApiClient;
import com.josala.demoapp.data.rest.LoggingInterceptorFactory;
import com.josala.demoapp.data.rest.OkHttpClientFactory;
import com.josala.demoapp.data.rest.QueryInterceptorFactory;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nomad on 01/04/2017.
 */
@Module
public class ApplicationModule {

    /**
     * The app context.
     */
    protected Context ctx;
    /**
     * The Backend url.
     */
    protected String baseUrl;

    /**
     * Instantiates a new Application module.
     *
     * @param application the application
     */
    public ApplicationModule(Application application, String baseUrl) {
        this.ctx = application.getApplicationContext();
        this.baseUrl = baseUrl;
    }

    /**
     * Provide application context context.
     *
     * @return the context
     */
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return ctx;
    }

    /**
     * Provide api url string.
     *
     * @return the string
     */
    @Provides
    @Named("apiUrl")
    public String provideApiUrl() {
        return baseUrl;
    }

    /**
     * Provide query interceptor factory query interceptor factory.
     *
     * @return the query interceptor factory
     */
    @Provides
    @Singleton
    QueryInterceptorFactory provideQueryInterceptorFactory() {
        return new QueryInterceptorFactory();
    }

    /**
     * Provide logging interceptor factory logging interceptor factory.
     *
     * @return the logging interceptor factory
     */
    @Provides
    @Singleton
    LoggingInterceptorFactory provideLoggingInterceptorFactory() {
        return new LoggingInterceptorFactory();
    }

    /**
     * Provide okhttp client ok http client factory.
     *
     * @param queryInterceptor   the query interceptor
     * @param loggingInterceptor the logging interceptor
     * @return the ok http client factory
     */
    @Provides
    OkHttpClientFactory provideOkhttpClient(QueryInterceptorFactory queryInterceptor,
                                            LoggingInterceptorFactory loggingInterceptor) {
        return new OkHttpClientFactory(loggingInterceptor.getLoggingInterceptor(),
                queryInterceptor.getQueryInterceptor());
    }

    /**
     * Provide subscriber scheduler scheduler.
     *
     * @return the scheduler
     */
    @Provides
    @Named("subscriber")
    @Singleton
    Scheduler provideSubscriberScheduler() {
        return Schedulers.io();
    }


    /**
     * Provide observer scheduler scheduler.
     *
     * @return the scheduler
     */
    @Provides
    @Named("observer")
    @Singleton
    Scheduler provideObserverScheduler() {
        return AndroidSchedulers.mainThread();
    }

    /**
     * Provide api client api client.
     *
     * @return the api client
     */
    @Provides
    @Singleton
    ApiClient provideApiClient() {
        OkHttpClientFactory okHttpClientFactory = provideOkhttpClient(
                provideQueryInterceptorFactory(),
                provideLoggingInterceptorFactory());
        return new ApiClient(baseUrl, okHttpClientFactory.getOkhttpClient());
    }

    /**
     * Provide the encrypt manager.
     *
     * @return the encrypt manager
     */
    @Provides
    @Singleton
    EncryptManager provideEncryptManager() {
        return new EncryptManager();
    }
}
