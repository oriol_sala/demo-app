package com.josala.demoapp.business;

import android.support.annotation.NonNull;

import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.data.rest.ApiClient;
import com.josala.demoapp.data.rest.entities.GetCharactersResponse;
import com.josala.demoapp.data.rest.mapper.CharacterMapper;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

/**
 * Created by nomad on 01/04/2017.
 */

public class GetCharactersUseCase extends AuthUseCase {

    private CharacterMapper characterMapper;
    private int offset = 0;

    /**
     * Constructor.
     *
     * @param apiClient
     * @param subscriberScheduler
     * @param observableScheduler
     * @param encryptManager
     * @param characterMapper
     */
    @Inject
    public GetCharactersUseCase(@NonNull ApiClient apiClient,
                                @Named("subscriber") @NonNull Scheduler subscriberScheduler,
                                @Named("observer") @NonNull Scheduler observableScheduler,
                                @NonNull EncryptManager encryptManager,
                                @NonNull CharacterMapper characterMapper) {
        super(apiClient, subscriberScheduler, observableScheduler, encryptManager);
        this.characterMapper = characterMapper;
    }

    public void setParameters(int offset) {
        this.offset = offset;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        if (offset > 0) {
            auth.put("offset", Integer.toString(offset));
        }
        return getResponse(apiClient.getCharacters(auth))
                .flatMap(new Func1<GetCharactersResponse, Observable<ArrayList<HeroOverView>>>() {
                    @Override
                    public Observable<ArrayList<HeroOverView>> call(
                            GetCharactersResponse getCharactersResponse) {
                        return Observable.just(characterMapper
                                .mapCharactersDataToArrayList(getCharactersResponse.getData()));
                    }
                });
    }
}
