package com.josala.demoapp.business;

import android.support.annotation.NonNull;

import com.josala.demoapp.data.rest.ApiClient;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by nomad on 01/04/2017.
 */

public abstract class AbstractUseCase<T> {

    protected final ApiClient apiClient;
    private final Scheduler subscriberScheduler;
    private final Scheduler observableScheduler;
    private Subscription subscription;

    /**
     * Constructor.
     *
     * @param apiClient           The apiClient to be used.
     * @param subscriberScheduler Subscribers scheduler.
     * @param observableScheduler Observables scheduler.
     */
    public AbstractUseCase(@NonNull ApiClient apiClient,
                           @NonNull Scheduler subscriberScheduler,
                           @NonNull Scheduler observableScheduler) {
        if (apiClient == null) {
            throw new IllegalArgumentException("ApiClient cannot be null");
        }
        if (subscriberScheduler == null) {
            throw new IllegalArgumentException("Subscriber scheduler cannot be null");
        }
        if (observableScheduler == null) {
            throw new IllegalArgumentException("Observable scheduler cannot be null");
        }

        this.apiClient = apiClient;
        this.subscriberScheduler = subscriberScheduler;
        this.observableScheduler = observableScheduler;
        subscription = Subscriptions.empty();
        subscription.unsubscribe();
    }

    /**
     * Builds the observable to subscribe on, here you must do
     * all the logic of the use case by using or/and combining observables.
     *
     * @return The observable to subscribe on.
     */
    protected abstract Observable<T> buildUseCaseObservable();

    /**
     * Subscribes a subscriber to the use case observable, replaces if any.
     *
     * @param subscriber Subscriber to observe observable.
     */
    public void subscribe(Subscriber<T> subscriber) {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        this.subscription = this.buildUseCaseObservable()
                .subscribeOn(subscriberScheduler)
                .observeOn(observableScheduler)
                .subscribe(subscriber);
    }


    /**
     * Unsubscribes from current {@link Subscription}.
     * Call this when you don't want to get any response
     */
    public void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

}
