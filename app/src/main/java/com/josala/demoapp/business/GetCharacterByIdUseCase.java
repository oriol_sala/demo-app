package com.josala.demoapp.business;

import android.support.annotation.NonNull;

import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.data.rest.ApiClient;
import com.josala.demoapp.data.rest.entities.Character;
import com.josala.demoapp.data.rest.entities.GetCharactersResponse;
import com.josala.demoapp.data.rest.mapper.CharacterMapper;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

/**
 * Created by nomad on 03/04/2017.
 */

public class GetCharacterByIdUseCase extends AuthUseCase {

    private CharacterMapper characterMapper;
    private int characterId;

    /**
     * @param apiClient
     * @param subscriberScheduler
     * @param observableScheduler
     * @param encryptManager
     * @param characterMapper
     */
    @Inject
    public GetCharacterByIdUseCase(@NonNull ApiClient apiClient,
                                   @Named("subscriber") @NonNull Scheduler subscriberScheduler,
                                   @Named("observer") @NonNull Scheduler observableScheduler,
                                   @NonNull EncryptManager encryptManager,
                                   @NonNull CharacterMapper characterMapper) {
        super(apiClient, subscriberScheduler, observableScheduler, encryptManager);
        this.characterMapper = characterMapper;
    }

    public void setParameters(int id) {
        characterId = id;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return getResponse(apiClient.getCharacterById(characterId, auth))
                .flatMap(new Func1<GetCharactersResponse, Observable<HeroDetail>>() {
                    @Override
                    public Observable<HeroDetail> call(
                            GetCharactersResponse getCharactersResponse) {
                        Character character = getCharactersResponse.getData().getResults().get(0);
                        return Observable.just(characterMapper.mapHero(character));
                    }
                });
    }
}
