package com.josala.demoapp.business;

import com.josala.demoapp.common.RealmManager;
import com.josala.demoapp.data.local.HeroDetail;

import javax.inject.Inject;

/**
 * Created by nomad on 04/04/2017.
 */

public class FavouriteCharacterUseCase {

    private RealmManager realmManager;

    @Inject
    public FavouriteCharacterUseCase(RealmManager realmManager) {
        this.realmManager = realmManager;
    }

    public void execute(HeroDetail hero) {
        realmManager.saveHero(hero);
    }
}
