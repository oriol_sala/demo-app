package com.josala.demoapp.business;

import android.support.annotation.NonNull;

import com.josala.demoapp.common.EncryptManager;
import com.josala.demoapp.data.rest.ApiClient;
import com.josala.demoapp.data.rest.entities.BaseResponse;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

/**
 * Created by nomad on 01/04/2017.
 */

public abstract class AuthUseCase extends AbstractUseCase {

    public final static int RESPONSE_OK = 200;
    protected Map<String, String> auth;

    public AuthUseCase(@NonNull ApiClient apiClient,
                       @NonNull Scheduler subscriberScheduler,
                       @NonNull Scheduler observableScheduler,
                       @NonNull EncryptManager encryptManager) {
        super(apiClient, subscriberScheduler, observableScheduler);
        String timestamp = new Date().toString();
        auth = new HashMap<>();
        auth.put("ts", timestamp);
        auth.put("apikey", EncryptManager.PUBLIC_KEY);
        auth.put("hash", encryptManager.getHash(timestamp));
    }

    /**
     * Api error mapping
     * @param response
     * @return
     */
    public Observable getResponse(Observable<? extends BaseResponse> response) {
        return response.flatMap(new Func1<BaseResponse, Observable<BaseResponse>>() {
            @Override
            public Observable<BaseResponse> call(BaseResponse baseResponse) {
                if (baseResponse.getCode() == RESPONSE_OK) {
                    return Observable.just(baseResponse);
                } else {
                    return Observable.error(new Exception(baseResponse.getStatus()));
                }
            }
        });
    }
}
