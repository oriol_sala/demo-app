package com.josala.demoapp.data.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.josala.demoapp.data.rest.entities.GetCharactersResponse;

import java.util.Map;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by nomad on 01/04/2017.
 */

public class ApiClient implements Api {

    private final OkHttpClient okHttpClient;
    private String baseUrl;
    private Api client;

    /**
     * Instantiates a new Api client.
     *
     * @param okHttpClient the ok http client
     */
    public ApiClient(String baseUrl, OkHttpClient okHttpClient) {
        this.baseUrl = baseUrl;
        this.okHttpClient = okHttpClient;
    }

    /**
     * Gets rest adapter.
     *
     * @return the rest adapter
     */
    public Api getRestAdapter() {
        if (client == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(getGsonConverter())
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .build();

            client = retrofit.create(Api.class);
        }
        return client;
    }

    /**
     * Gets the gson converter used to parse API responses.
     */
    private GsonConverterFactory getGsonConverter() {
        Gson gson = new GsonBuilder().create();
        return GsonConverterFactory.create(gson);
    }

    @Override
    public Observable<GetCharactersResponse> getCharacters(@QueryMap Map<String, String> auth) {
        return getRestAdapter().getCharacters(auth);
    }

    @Override
    public Observable<GetCharactersResponse> getCharacterById(@Path("id") int characterId,
                                                              @QueryMap Map<String, String> auth) {
        return getRestAdapter().getCharacterById(characterId, auth);
    }
}
