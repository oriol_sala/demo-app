package com.josala.demoapp.data.rest;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by nomad on 01/04/2017.
 */

public class QueryInterceptorFactory {

    /**
     * Gets query interceptor.
     *
     * @return the query interceptor
     */
    public Interceptor getQueryInterceptor() {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        };
        return interceptor;
    }
}
