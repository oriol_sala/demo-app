package com.josala.demoapp.data.rest;

import com.josala.demoapp.BuildConfig;

import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by nomad on 01/04/2017.
 */

public class LoggingInterceptorFactory {

    /**
     * Gets logging interceptor.
     *
     * @return the logging interceptor
     */
    public Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return logging;
    }
}
