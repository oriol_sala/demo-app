package com.josala.demoapp.data.rest.mapper;

import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.data.rest.entities.Character;
import com.josala.demoapp.data.rest.entities.CharactersData;
import com.josala.demoapp.data.rest.entities.Thumbnail;

import java.util.ArrayList;


/**
 * Created by nomad on 02/04/2017.
 */

public class CharacterMapper {

    public ArrayList<HeroOverView> mapCharactersDataToArrayList(CharactersData data) {
        int totalCharacters = data.getResults().size();
        ArrayList<HeroOverView> res = new ArrayList<>();
        for (int i = 0; i < totalCharacters; i++) {
            HeroOverView h = mapCharacter(data.getResults().get(i));
            res.add(h);
        }
        return res;
    }

    private String mapThumbNail(Thumbnail tn) {
        return tn.getPath() + "." + tn.getExtension();
    }

    public HeroOverView mapCharacter(Character data) {
        HeroOverView res = new HeroOverView();
        res.setId(data.getId());
        res.setName(data.getName());
        res.setThumbnail(mapThumbNail(data.getThumbnail()));
        return res;
    }

    public HeroDetail mapHero(Character data) {
        HeroDetail res = new HeroDetail();
        res.setId(data.getId());
        res.setName(data.getName());
        res.setDescription(data.getDescription());
        res.setThumbnail(mapThumbNail(data.getThumbnail()));
        return res;
    }
}
