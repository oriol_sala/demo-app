package com.josala.demoapp.data.rest;

import com.josala.demoapp.data.rest.entities.GetCharactersResponse;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by nomad on 01/04/2017.
 */

public interface Api {

    @GET("characters")
    Observable<GetCharactersResponse> getCharacters(@QueryMap Map<String, String> auth);

    @GET("characters/{id}")
    Observable<GetCharactersResponse> getCharacterById(@Path("id") int groupId,
                                                    @QueryMap Map<String, String> auth);
}
