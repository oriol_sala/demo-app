package com.josala.demoapp.data.rest.entities;

/**
 * Created by nomad on 01/04/2017.
 */

public class GetCharactersResponse extends BaseResponse {

    private CharactersData data;

    public CharactersData getData() {
        return data;
    }

    public void setData(CharactersData data) {
        this.data = data;
    }
}
