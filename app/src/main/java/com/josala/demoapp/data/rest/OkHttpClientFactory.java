package com.josala.demoapp.data.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

/**
 * Created by nomad on 01/04/2017.
 */

public class OkHttpClientFactory {

    private static final int CONNECTION_TIMEOUT = 10000;

    private final Interceptor loggingInterceptor;
    private final Interceptor queryInterceptor;

    /**
     * Instantiates a new Ok http client factory.
     *
     * @param logging the logging
     * @param query   the query
     */
    public OkHttpClientFactory(Interceptor logging, Interceptor query) {
        this.loggingInterceptor = logging;
        this.queryInterceptor = query;
    }

    /**
     * Gets okhttp client.
     *
     * @return the okhttp client
     */
    public OkHttpClient getOkhttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(loggingInterceptor);
        httpClient.addInterceptor(queryInterceptor);
        httpClient.retryOnConnectionFailure(true);
        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        return httpClient.build();
    }
}
