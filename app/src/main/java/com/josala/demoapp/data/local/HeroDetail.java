package com.josala.demoapp.data.local;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nomad on 04/04/2017.
 */

public class HeroDetail extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id;
    private String name;
    private String thumbnail;
    private String description;
    private boolean isFavourite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.thumbnail);
        dest.writeString(this.description);
        dest.writeByte(this.isFavourite ? (byte) 1 : (byte) 0);
    }

    public HeroDetail() {
    }

    protected HeroDetail(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.thumbnail = in.readString();
        this.description = in.readString();
        this.isFavourite = in.readByte() != 0;
    }

    public static final Parcelable.Creator<HeroDetail> CREATOR = new Parcelable.Creator<HeroDetail>() {
        @Override
        public HeroDetail createFromParcel(Parcel source) {
            return new HeroDetail(source);
        }

        @Override
        public HeroDetail[] newArray(int size) {
            return new HeroDetail[size];
        }
    };
}
