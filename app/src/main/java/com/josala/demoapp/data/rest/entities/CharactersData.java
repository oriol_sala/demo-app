package com.josala.demoapp.data.rest.entities;

import java.util.List;

/**
 * Created by nomad on 01/04/2017.
 */

public class CharactersData extends Data {

    private List<Character> results;

    public List<Character> getResults() {
        return results;
    }

    public void setResults(List<Character> results) {
        this.results = results;
    }
}
