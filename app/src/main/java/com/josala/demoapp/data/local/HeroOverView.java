package com.josala.demoapp.data.local;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nomad on 02/04/2017.
 */

public class HeroOverView implements Parcelable {

    private int id;
    private String name;
    private String thumbnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.thumbnail);
    }

    public HeroOverView() {
    }

    protected HeroOverView(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.thumbnail = in.readString();
    }

    public static final Parcelable.Creator<HeroOverView> CREATOR =
            new Parcelable.Creator<HeroOverView>() {
        @Override
        public HeroOverView createFromParcel(Parcel source) {
            return new HeroOverView(source);
        }

        @Override
        public HeroOverView[] newArray(int size) {
            return new HeroOverView[size];
        }
    };
}
