package com.josala.demoapp.view.activity;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.josala.demoapp.di.module.ActivityModule;
import com.josala.demoapp.presenter.BasePresenter;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by nomad on 02/04/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private ActivityModule activityModule;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        unbinder = ButterKnife.bind(this);
        injectDependencies(savedInstanceState);
        if (getPresenter() != null) {
            getPresenter().attachView(this);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().destroy();
        }
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    /**
     * Gets context module.
     *
     * @return the context module
     */
    public ActivityModule getActivityModule() {
        if (activityModule == null) {
            activityModule = new ActivityModule(this);
        }
        return activityModule;
    }

    /**
     * The screen layout to inflate.
     *
     * @return the layout
     */
    protected abstract int getLayout();

    /**
     * Gets presenter.
     *
     * @return the presenter
     */
    protected abstract BasePresenter getPresenter();

    /**
     * Inject dependencies.
     *
     * @param savedInstanceState the saved instance state
     */
    public abstract void injectDependencies(Bundle savedInstanceState);

    @Override
    public void showMessage(@StringRes int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
    }
}
