package com.josala.demoapp.view.activity;

/**
 * Created by nomad on 02/04/2017.
 */

public interface SplashView extends BaseView {

    void fadeOutLogo();

    void finishApp();
}
