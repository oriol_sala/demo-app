package com.josala.demoapp.view.activity;

import com.josala.demoapp.data.local.HeroOverView;

import java.util.ArrayList;

/**
 * Created by nomad on 02/04/2017.
 */

public interface HomeView extends BaseView {

    void expandCharactersList(ArrayList<HeroOverView> characters);
}
