package com.josala.demoapp.view.activity;

import android.support.annotation.StringRes;

/**
 * Created by nomad on 02/04/2017.
 */

public interface BaseView {

    /**
     * Show message.
     *
     * @param resId the text res id
     */
    void showMessage(@StringRes int resId);
}
