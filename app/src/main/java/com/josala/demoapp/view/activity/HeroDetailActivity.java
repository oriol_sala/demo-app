package com.josala.demoapp.view.activity;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.josala.demoapp.DemoApp;
import com.josala.demoapp.R;
import com.josala.demoapp.common.Constants;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.di.component.DaggerActivityComponent;
import com.josala.demoapp.di.module.ActivityModule;
import com.josala.demoapp.presenter.BasePresenter;
import com.josala.demoapp.presenter.HeroDetailPresenter;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by nomad on 04/04/2017.
 */

public class HeroDetailActivity extends BaseActivity implements HeroDetailView {

    @BindView(R.id.ivHero)
    ImageView ivHero;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.btnFavourite)
    ImageButton btnFavourite;
    @Inject
    HeroDetailPresenter heroDetailPresenter;

    @Override
    protected int getLayout() {
        return R.layout.activity_hero_detail;
    }

    @Override
    protected BasePresenter getPresenter() {
        return heroDetailPresenter;
    }

    @Override
    public void injectDependencies(Bundle savedInstanceState) {
        DaggerActivityComponent.builder()
                .applicationComponent(DemoApp.getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HeroDetail data = getIntent().getParcelableExtra(Constants.ARG_HERO_DETAIL);
        heroDetailPresenter.setData(data);
        displayData(data);
    }

    private void displayData(HeroDetail data) {
        tvName.setText(data.getName());
        tvDescription.setText(data.getDescription());
        Picasso picasso = new Picasso.Builder(this).build();
        picasso.load(data.getThumbnail()).into(ivHero);
    }

    @OnClick(R.id.btnFavourite)
    public void toggleFavourite() {
        heroDetailPresenter.toggleFavourite();
    }

    @Override
    public void enableFavouriteSign(boolean isFavourite) {
        btnFavourite.setImageDrawable(isFavourite ? getResources().getDrawable(android.R.drawable.star_big_on) :
                getResources().getDrawable(android.R.drawable.star_big_off));
    }

    @Override
    public void showFavouriteMessage(boolean isFavourite) {
        showMessage(isFavourite ? R.string.added_to_favourite : R.string.removed_from_favourites);
    }
}
