package com.josala.demoapp.view.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.josala.demoapp.DemoApp;
import com.josala.demoapp.R;
import com.josala.demoapp.common.Constants;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.di.component.DaggerActivityComponent;
import com.josala.demoapp.di.module.ActivityModule;
import com.josala.demoapp.presenter.BasePresenter;
import com.josala.demoapp.presenter.HomePresenter;
import com.josala.demoapp.view.adapter.CharactersGridAdapter;
import com.josala.demoapp.view.adapter.ResponsiveGridLayoutManager;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by nomad on 02/04/2017.
 */

public class HomeActivity extends BaseActivity implements HomeView {

    @BindView(R.id.rvCharacters)
    RecyclerView rvCharacters;
    @Inject
    HomePresenter homePresenter;
    @Inject
    CharactersGridAdapter gridAdapter;

    @Override
    protected int getLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected BasePresenter getPresenter() {
        return homePresenter;
    }

    @Override
    public void injectDependencies(Bundle savedInstanceState) {
        DaggerActivityComponent.builder()
                .applicationComponent(DemoApp.getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<HeroOverView> data = getIntent()
                .getParcelableArrayListExtra(Constants.ARG_INITIAL_DATA);
        initGridView(data);
    }

    private void initGridView(ArrayList<HeroOverView> data) {
        gridAdapter.setListener(homePresenter);
        int minItemWidth = 300;
        rvCharacters.setLayoutManager(new ResponsiveGridLayoutManager(this, minItemWidth));
        rvCharacters.setAdapter(gridAdapter);
        gridAdapter.setData(data);
        rvCharacters.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int visibleItemCount = rvCharacters.getLayoutManager().getChildCount();
                int totalItemCount = rvCharacters.getLayoutManager().getItemCount();
                int pastVisiblesItems = ((LinearLayoutManager)recyclerView.getLayoutManager())
                        .findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    homePresenter.getCharacters(totalItemCount);
                }

            }
        });
    }

    @Override
    public void expandCharactersList(ArrayList<HeroOverView> characters) {
        gridAdapter.expandList(characters);
    }
}
