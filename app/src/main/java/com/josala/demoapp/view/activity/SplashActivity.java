package com.josala.demoapp.view.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.widget.ImageView;

import com.josala.demoapp.DemoApp;
import com.josala.demoapp.R;
import com.josala.demoapp.di.component.DaggerActivityComponent;
import com.josala.demoapp.di.module.ActivityModule;
import com.josala.demoapp.presenter.BasePresenter;
import com.josala.demoapp.presenter.SplashPresenter;

import javax.inject.Inject;

import butterknife.BindView;

public class SplashActivity extends BaseActivity implements SplashView {

    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @Inject
    SplashPresenter splashPresenter;

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected BasePresenter getPresenter() {
        return splashPresenter;
    }

    @Override
    public void injectDependencies(Bundle savedInstanceState) {
        DaggerActivityComponent.builder()
                .applicationComponent(DemoApp.getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashPresenter.getCharacters();
    }

    @Override
    public void finishApp() {
        this.finish();
    }

    @Override
    public void fadeOutLogo() {
        ivLogo.animate()
                .alpha(0.0f)
                .setDuration(getResources().getInteger(R.integer.animationLong))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        splashPresenter.goToMain();
                    }
                });
    }
}
