package com.josala.demoapp.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josala.demoapp.R;
import com.josala.demoapp.common.ImageLoader;
import com.josala.demoapp.data.local.HeroOverView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nomad on 03/04/2017.
 */

public class CharactersGridAdapter extends RecyclerView.Adapter<CharactersGridAdapter.CharacterItemViewHolder> {

    private ArrayList<HeroOverView> data;
    private LayoutInflater inflater;
    private OnHeroListener listener;
    private Picasso picasso;

    public CharactersGridAdapter(Context context,
                                 ImageLoader imageLoader) {
        inflater = LayoutInflater.from(context);
        picasso = imageLoader.getImageLoader(context);
        data = new ArrayList<>();
    }

    /**
     * Sets the array of data and notifies.
     *
     * @param data
     */
    public void setData(ArrayList<HeroOverView> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * Expands the current list with the given list.
     *
     * @param extraData
     */
    public void expandList(ArrayList<HeroOverView> extraData) {
        data.addAll(extraData);
        notifyDataSetChanged();
    }

    /**
     * Sets the outter listener.
     *
     * @param listener
     */
    public void setListener(OnHeroListener listener) {
        this.listener = listener;
    }

    @Override
    public CharacterItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.character_grid_item, parent, false);
        return new CharacterItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CharacterItemViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CharacterItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * The tv for the hero name.
         */
        @BindView(R.id.tvHeroName)
        TextView tvHeroName;

        /**
         * The iv for the hero thumbnail.
         */
        @BindView(R.id.ivHeroThumbnail)
        ImageView ivHeroThumbnail;

        public CharacterItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bindData(HeroOverView heroOverView) {
            tvHeroName.setText(heroOverView.getName());
            picasso.load(heroOverView.getThumbnail()).into(ivHeroThumbnail);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            int id = data.get(position).getId();
            listener.onHeroClick(id);
        }
    }

    public interface OnHeroListener {

        void onHeroClick(int id);
    }
}
