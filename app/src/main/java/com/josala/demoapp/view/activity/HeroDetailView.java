package com.josala.demoapp.view.activity;

/**
 * Created by nomad on 04/04/2017.
 */

public interface HeroDetailView extends BaseView {

    void enableFavouriteSign(boolean isFavourite);

    void showFavouriteMessage(boolean isFavourite);
}
