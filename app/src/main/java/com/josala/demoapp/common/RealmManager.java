package com.josala.demoapp.common;

import com.josala.demoapp.data.local.HeroDetail;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by nomad on 04/04/2017.
 */

public class RealmManager {

    private final static int SCHEMA_VERSION = 1;
    private final static String DB_NAME = "demoappdb";

    public RealmManager() {
        RealmConfiguration.Builder configBuilder = new RealmConfiguration.Builder()
                .schemaVersion(SCHEMA_VERSION)
                .name(DB_NAME);
        Realm.setDefaultConfiguration(configBuilder.build());
    }

    /**
     * Gets the hero with the given id, returns null if does not exist.
     *
     * @param id
     * @return
     */
    public HeroDetail getHeroById(int id) {
        Realm realm = Realm.getDefaultInstance();
        final HeroDetail storedHero = realm.where(HeroDetail.class)
                .equalTo(Constants.REALM_HERO_ID, id)
                .findFirst();
        return storedHero;
    }

    /**
     * Is hero favourite boolean.
     *
     * @param hero the hero
     * @return the boolean
     */
    public boolean isHeroFavourite(HeroDetail hero) {
        Realm realm = Realm.getDefaultInstance();
        final HeroDetail storedHero = realm.where(HeroDetail.class)
                .equalTo(Constants.REALM_HERO_ID, hero.getId())
                .equalTo(Constants.REALM_HERO_FAVOURITE, true)
                .findFirst();
        return storedHero == null ? false : true;
    }

    /**
     * Save or updates the given hero.
     *
     * @param hero
     */
    public void saveHero(HeroDetail hero) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(hero);
        realm.commitTransaction();
    }

}
