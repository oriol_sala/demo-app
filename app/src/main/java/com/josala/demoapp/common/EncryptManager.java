package com.josala.demoapp.common;

import android.util.Log;

import com.josala.demoapp.BuildConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by nomad on 01/04/2017.
 */

public class EncryptManager {

    private final static String TAG = EncryptManager.class.getName();
    /**
     * The api public key.
     */
    public final static String PUBLIC_KEY = "38c5805229160659336c16dcc4b755c4";

    /**
     * returns the md5 digest of (ts+privatekey+publickey)
     *
     * @param ts
     * @return
     */
    public String getHash(String ts) {
        StringBuilder sb = new StringBuilder();
        sb.append(ts);
        sb.append(BuildConfig.PK);
        sb.append(PUBLIC_KEY);
        String msg = sb.toString();
        return md5(msg);
    }

    public String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return "";
    }
}
