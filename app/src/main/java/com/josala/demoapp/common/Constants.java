package com.josala.demoapp.common;

/**
 * Created by nomad on 02/04/2017.
 */

public class Constants {

    public final static String ARG_INITIAL_DATA = "noloaders";
    public final static String ARG_HERO_DETAIL = "herodetail";

    public final static String REALM_HERO_ID = "id";
    public final static String REALM_HERO_NAME = "name";
    public final static String REALM_HERO_THUMBNAIL = "thumbnail";
    public final static String REALM_HERO_DESC = "description";
    public final static String REALM_HERO_FAVOURITE = "isFavourite";
}
