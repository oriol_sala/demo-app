package com.josala.demoapp.common;

import android.content.Context;

import com.squareup.picasso.Picasso;

/**
 * Created by nomad on 03/04/2017.
 */

public class ImageLoader {

    public Picasso getImageLoader(Context context) {
        Picasso picasso = new Picasso.Builder(context).build();
        return picasso;
    }
}
