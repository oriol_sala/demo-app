package com.josala.demoapp.common;

import android.app.Activity;
import android.content.Intent;

import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.view.activity.HeroDetailActivity;
import com.josala.demoapp.view.activity.HomeActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by nomad on 02/04/2017.
 */

public class Navigator {

    private final WeakReference<Activity> activity;

    /**
     * Instantiates a new Navigator.
     *
     * @param activity the context
     */
    @Inject
    public Navigator(Activity activity) {
        this.activity = new WeakReference<>(activity);
    }

    /**
     * Navigates to home activity with data and clears the stack.
     */
    public void navigateToHomeActivity(ArrayList<HeroOverView> data) {
        Intent intent = new Intent(activity.get(), HomeActivity.class);
        intent.putExtra(Constants.ARG_INITIAL_DATA, data);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.get().startActivity(intent);
    }

    /**
     * Navigates to hero detail activity with partial data.
     * @param data
     */
    public void navigateToHeroDetailActivity(HeroDetail data) {
        Intent intent = new Intent(activity.get(), HeroDetailActivity.class);
        intent.putExtra(Constants.ARG_HERO_DETAIL, data);
        activity.get().startActivity(intent);
    }
}
