package com.josala.demoapp;

import android.app.Application;

import com.josala.demoapp.di.component.ApplicationComponent;
import com.josala.demoapp.di.component.DaggerApplicationComponent;
import com.josala.demoapp.di.module.ApplicationModule;

import io.realm.Realm;

/**
 * Created by nomad on 01/04/2017.
 */

public class DemoApp extends Application {

    /**
     * The application Component.
     */
    protected static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
        applicationComponent.inject(this);
        Realm.init(this);
    }

    protected void setupGraph() {
        String baseUrl = getResources().getString(R.string.baseUrl);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this, baseUrl))
                .build();
    }

    /**
     * Gets application component.
     *
     * @return the application component
     */
    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
