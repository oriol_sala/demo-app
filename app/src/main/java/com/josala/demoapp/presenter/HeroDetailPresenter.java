package com.josala.demoapp.presenter;

import com.josala.demoapp.business.FavouriteCharacterUseCase;
import com.josala.demoapp.common.RealmManager;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.view.activity.HeroDetailView;

/**
 * Created by nomad on 04/04/2017.
 */

public class HeroDetailPresenter extends BasePresenter<HeroDetailView> {

    private FavouriteCharacterUseCase favouriteCharacterUseCase;
    private HeroDetail mHero;
    private RealmManager realmManager;

    public HeroDetailPresenter(FavouriteCharacterUseCase favouriteCharacterUseCase,
                               RealmManager realmManager) {
        this.favouriteCharacterUseCase = favouriteCharacterUseCase;
        this.realmManager = realmManager;
    }

    @Override
    protected void unsubscribe() {

    }

    /**
     * Sets the hero model.
     *
     * @param heroDetail
     */
    public void setData(HeroDetail heroDetail) {
        mHero = heroDetail;
        boolean isFavourite = realmManager.isHeroFavourite(heroDetail);
        mHero.setFavourite(isFavourite);
        getView().enableFavouriteSign(isFavourite);
    }

    /**
     * Toggles favourite status.
     */
    public void toggleFavourite() {

        boolean isFavourite = !mHero.isFavourite();
        mHero.setFavourite(isFavourite);
        getView().enableFavouriteSign(isFavourite);
        getView().showFavouriteMessage(isFavourite);
        favouriteCharacterUseCase.execute(mHero);
    }

}
