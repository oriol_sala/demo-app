package com.josala.demoapp.presenter;

import com.josala.demoapp.R;
import com.josala.demoapp.business.GetCharacterByIdUseCase;
import com.josala.demoapp.business.GetCharactersUseCase;
import com.josala.demoapp.common.Navigator;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.view.activity.HomeView;
import com.josala.demoapp.view.adapter.CharactersGridAdapter;

import java.util.ArrayList;

import rx.Subscriber;

/**
 * Created by nomad on 03/04/2017.
 */

public class HomePresenter extends BasePresenter<HomeView> implements CharactersGridAdapter.OnHeroListener {

    private GetCharactersUseCase getCharactersUseCase;
    private GetCharacterByIdUseCase getCharacterByIdUseCase;
    private Navigator navigator;

    public HomePresenter(GetCharactersUseCase getCharactersUseCase,
                         GetCharacterByIdUseCase getCharacterByIdUseCase,
                         Navigator navigator) {
        this.getCharactersUseCase = getCharactersUseCase;
        this.getCharacterByIdUseCase = getCharacterByIdUseCase;
        this.navigator = navigator;
    }

    @Override
    protected void unsubscribe() {
        getCharacterByIdUseCase.unsubscribe();
        getCharactersUseCase.unsubscribe();
    }

    @Override
    public void onHeroClick(int id) {
        getCharacterByIdUseCase.setParameters(id);
        getCharacterByIdUseCase.subscribe(new GetCharacterByIdSubscriber());
    }

    /**
     * Makes a get characters request with the given offset
     * @param offset
     */
    public void getCharacters(int offset) {
        getCharactersUseCase.setParameters(offset);
        getCharactersUseCase.subscribe(new GetCharactersSubscriber());
    }

    private class GetCharactersSubscriber extends Subscriber<ArrayList<HeroOverView>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(ArrayList<HeroOverView> mappedResponse) {
            getView().expandCharactersList(mappedResponse);
        }
    }

    private class GetCharacterByIdSubscriber extends Subscriber<HeroDetail> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            getView().showMessage(R.string.error_generic);
        }

        @Override
        public void onNext(HeroDetail mappedResponse) {
            navigator.navigateToHeroDetailActivity(mappedResponse);
        }
    }
}
