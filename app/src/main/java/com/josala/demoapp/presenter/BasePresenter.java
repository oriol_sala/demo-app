package com.josala.demoapp.presenter;

import android.support.annotation.NonNull;

import com.josala.demoapp.view.activity.BaseView;

import java.lang.ref.WeakReference;

/**
 * Created by nomad on 02/04/2017.
 */

public abstract class BasePresenter<V extends BaseView> {

    /**
     * The View.
     */
    protected WeakReference<V> view;

    /**
     * Called when the context is destroyed.
     */
    public void destroy() {
        if (view != null) {
            view.clear();
            view = null;
        }
    }

    /**
     * Unsubscribe your use cases here.
     */
    protected abstract void unsubscribe();

    /**
     * Attaches the view to the presenter.
     *
     * @param view The view to attach.
     */
    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    @NonNull
    public V getView() {
        if (view == null) {
            throw new IllegalArgumentException("View not attached: " + this);
        } else {
            return this.view.get();
        }
    }
}
