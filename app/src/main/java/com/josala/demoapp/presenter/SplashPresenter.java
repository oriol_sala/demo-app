package com.josala.demoapp.presenter;

import com.josala.demoapp.R;
import com.josala.demoapp.business.GetCharactersUseCase;
import com.josala.demoapp.common.Navigator;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.view.activity.SplashView;

import java.util.ArrayList;

import rx.Subscriber;

/**
 * Created by nomad on 01/04/2017.
 */

public class SplashPresenter extends BasePresenter<SplashView> {

    private GetCharactersUseCase getCharactersUseCase;
    private Navigator navigator;
    private ArrayList<HeroOverView> data;

    public SplashPresenter(GetCharactersUseCase getCharactersUseCase,
                           Navigator navigator) {
        this.getCharactersUseCase = getCharactersUseCase;
        this.navigator = navigator;
    }

    @Override
    protected void unsubscribe() {
        getCharactersUseCase.unsubscribe();
    }

    /**
     * Network request to retrieve characters.
     */
    public void getCharacters() {
        getCharactersUseCase.subscribe(new GetCharactersSubscriber());
    }

    /**
     * Navigates to main activity
     */
    public void goToMain() {
        navigator.navigateToHomeActivity(data);
    }

    private class GetCharactersSubscriber extends Subscriber<ArrayList<HeroOverView>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            getView().showMessage(R.string.error_generic);
            getView().finishApp();
        }

        @Override
        public void onNext(ArrayList<HeroOverView> mappedResponse) {
            getView().fadeOutLogo();
            data = mappedResponse;
        }
    }
}
