package com.josala.demoapp;

import com.josala.demoapp.business.FavouriteCharacterUseCase;
import com.josala.demoapp.common.RealmManager;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.presenter.HeroDetailPresenter;
import com.josala.demoapp.view.activity.HeroDetailView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by nomad on 05/04/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class HeroDetailTest {

    private HeroDetailPresenter heroDetailPresenter;
    @Mock
    private HeroDetailView mockView;
    @Mock
    private FavouriteCharacterUseCase mockUseCase;
    @Mock
    private RealmManager mockRealm;

    @Before
    public void setUp() {
        heroDetailPresenter = new HeroDetailPresenter(mockUseCase, mockRealm);
        heroDetailPresenter.attachView(mockView);
    }

    @Test
    public void testSetData() {
        boolean isFav = true;
        int id = 123;
        HeroDetail mock = new HeroDetail();
        mock.setId(id);
        when(mockRealm.isHeroFavourite(mock)).thenReturn(isFav);
        heroDetailPresenter.setData(mock);
        verify(mockView).enableFavouriteSign(isFav);
    }

    @Test
    public void testToggleFavourite() {
        boolean isFav = true;
        int id = 123;
        HeroDetail mock = new HeroDetail();
        mock.setId(id);
        mock.setFavourite(isFav);
        when(mockRealm.isHeroFavourite(mock)).thenReturn(isFav);
        heroDetailPresenter.setData(mock);
        heroDetailPresenter.toggleFavourite();
        verify(mockView).enableFavouriteSign(!isFav);
        verify(mockView).showFavouriteMessage(!isFav);
        verify(mockUseCase).execute(mock);
    }
}
