package com.josala.demoapp;

import com.josala.demoapp.business.GetCharactersUseCase;
import com.josala.demoapp.common.Navigator;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.presenter.SplashPresenter;
import com.josala.demoapp.view.activity.SplashView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import rx.Subscriber;

import static org.mockito.Mockito.verify;

/**
 * Created by nomad on 02/04/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SplashPresenterTest {

    private SplashPresenter presenter;
    @Mock
    private GetCharactersUseCase mockUseCase;
    @Mock
    private SplashView mockSplashView;
    @Mock
    private Navigator mockNavigator;
    @Captor
    private ArgumentCaptor<Subscriber<ArrayList<HeroOverView>>> getCharactersCaptor;

    @Before
    public void setUp() {
        presenter = new SplashPresenter(mockUseCase, mockNavigator);
        presenter.attachView(mockSplashView);
    }

    @Test
    public void testGetCharactersSuccess() {

        ArrayList<HeroOverView> mockRes = new ArrayList<>();
        presenter.getCharacters();
        verify(mockUseCase).subscribe(getCharactersCaptor.capture());
        getCharactersCaptor.getValue().onNext(mockRes);
        verify(mockSplashView).fadeOutLogo();
        presenter.goToMain();
        verify(mockNavigator).navigateToHomeActivity(mockRes);
    }

    @Test
    public void testGetCharactersError() {

        presenter.getCharacters();
        verify(mockUseCase).subscribe(getCharactersCaptor.capture());
        getCharactersCaptor.getValue().onError(new Exception("something went wrong"));
        verify(mockSplashView).showMessage(R.string.error_generic);
        verify(mockSplashView).finishApp();
    }

}
