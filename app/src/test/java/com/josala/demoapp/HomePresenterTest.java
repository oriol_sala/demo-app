package com.josala.demoapp;

import com.josala.demoapp.business.GetCharacterByIdUseCase;
import com.josala.demoapp.business.GetCharactersUseCase;
import com.josala.demoapp.common.Navigator;
import com.josala.demoapp.data.local.HeroDetail;
import com.josala.demoapp.data.local.HeroOverView;
import com.josala.demoapp.presenter.HomePresenter;
import com.josala.demoapp.view.activity.HomeView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import rx.Subscriber;

import static org.mockito.Mockito.verify;

/**
 * Created by nomad on 05/04/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    private HomePresenter homePresenter;
    @Mock
    private HomeView mockView;
    @Mock
    private GetCharactersUseCase mockCharactersUseCase;
    @Mock
    private GetCharacterByIdUseCase mockCharacterByIdUseCase;
    @Mock
    private Navigator navigator;
    @Captor
    private ArgumentCaptor<Subscriber<ArrayList<HeroOverView>>> getCharactersCaptor;
    @Captor
    private ArgumentCaptor<Subscriber<HeroDetail>> getCharacterCaptor;

    @Before
    public void setUp() {
        homePresenter = new HomePresenter(mockCharactersUseCase, mockCharacterByIdUseCase, navigator);
        homePresenter.attachView(mockView);
    }

    @Test
    public void testOnHeroDetail() {
        HeroDetail mockHero = new HeroDetail();
        int id = 123;
        homePresenter.onHeroClick(id);
        verify(mockCharacterByIdUseCase).subscribe(getCharacterCaptor.capture());
        getCharacterCaptor.getValue().onNext(mockHero);
        verify(navigator).navigateToHeroDetailActivity(mockHero);
    }

    @Test
    public void testOnScrollBottom() {
        ArrayList<HeroOverView> mockResponse = new ArrayList<>();
        int offset = 20;
        homePresenter.getCharacters(offset);
        verify(mockCharactersUseCase).subscribe(getCharactersCaptor.capture());
        getCharactersCaptor.getValue().onNext(mockResponse);
        verify(mockView).expandCharactersList(mockResponse);
    }
}
